<?php
/**
 * Created by PhpStorm.
 * User: Niko
 * Date: 02.01.2019
 * Time: 13:10
 */

namespace App\Config;

class AppConfig {
    const APP_NAME='Currency Converter';
    const DB_NAME='currencies';
    const LAYOUT_DEFAULT='index.phtml';
    const ACTIVE_PROVIDER='XeCom';
    const PROVIDERS_INFO_DIRECTORY='providers_info';
    private $Providers = [
        'XeCom',
    ];

    public function getSystemPath(){
        return realpath( '../' );
    }

    public function getLayoutPath(){
        return $this->getSystemPath().DIRECTORY_SEPARATOR.'src'.DIRECTORY_SEPARATOR.'View'.DIRECTORY_SEPARATOR;
    }
    public function getProvidersInfoPath(){
        return $this->getSystemPath().DIRECTORY_SEPARATOR.'src'.DIRECTORY_SEPARATOR.'Config'.DIRECTORY_SEPARATOR.$this::PROVIDERS_INFO_DIRECTORY.DIRECTORY_SEPARATOR;
    }
    /** get Active Provider
     */
    public function getActiveProvider(){
        try{
            if(file_exists($this->getProvidersInfoPath().'active_provider'))//get active provider
            {
                $provider = file_get_contents($this->getProvidersInfoPath().'active_provider');
                if(in_array($provider,$this->Providers))
                    return $provider;
                else//if file content was changed manually and provider not found -> edit manually
                {
                    file_put_contents($this->getProvidersInfoPath().'active_provider',$this::ACTIVE_PROVIDER);
                    return $this::ACTIVE_PROVIDER;
                }
            }else//set active default and write file
            {
                file_put_contents($this->getProvidersInfoPath().'active_provider',$this::ACTIVE_PROVIDER);
                return $this::ACTIVE_PROVIDER;
            }
        }
        catch(\Exception $exception){

            /*TODO*/
        }

    }

    public function setActiveProvider($provider){

        if(in_array($provider,$this->Providers))
            file_put_contents($this->getProvidersInfoPath().'active_provider',$provider);

    }
    public function getProviders(){

        return $this->Providers;

    }

}