<?php
/**
 * Created by PhpStorm.
 * User: Niko
 * Date: 02.01.2019
 * Time: 13:01
 */

namespace App\Controller;
use App\Config\AppConfig;
use App\Model\Converter;


class IndexController
{
    public function index(){
        $config = new AppConfig();
        $active_provider = $config->getActiveProvider();
        $provider = 'App\Providers\Provider'.$active_provider;
        $provider = new $provider();

        $currencies = $provider->getCurrenciesList();

        if(isset($_POST['convert']))
        {
            $money = $provider->getExchange($_POST['from'],$_POST['to'],$_POST['amount']);

        }

        require_once $config->getLayoutPath() .'header.phtml';
        require_once $config->getLayoutPath() .'index.phtml';
        require_once $config->getLayoutPath() .'footer.phtml';

    }

    public function convert(){

        $config = new AppConfig();
        $active_provider = $config->getActiveProvider();
        $provider = 'App\Providers\Provider'.$active_provider;
        $provider = new $provider();

        if(isset($_GET['amount']))
        {
            $money = $provider->getExchange($_GET['from'],$_GET['to'],$_GET['amount']);
            $processed = 1;
            echo json_encode(['money'=>$money,'processed'=>$processed, 'from'=>$_GET['from'], 'to'=> $_GET['to'], 'amount'=>$_GET['amount']]);
        }else
        {
            $money = NULL;
            $processed = 0;
            echo json_encode(['money'=>$money,'processed'=>$processed]);
        }




    }

}