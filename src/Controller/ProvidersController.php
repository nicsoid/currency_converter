<?php
/**
 * Created by PhpStorm.
 * User: Niko
 * Date: 02.01.2019
 * Time: 14:51
 */

namespace App\Controller;
use App\Config\AppConfig;
use App\Model\Converter;

class ProvidersController
{
    public function index(){
        $config = new AppConfig();

        if(isset($_POST['submit']))
        {
            $config->setActiveProvider(htmlspecialchars($_POST['provider']));
        }

        $providers = $config->getProviders();
        $active_provider = $config->getActiveProvider();

        require_once $config->getLayoutPath() .'header.phtml';
        require_once $config->getLayoutPath() .'providers'.DIRECTORY_SEPARATOR.'index.phtml';
        require_once $config->getLayoutPath() .'footer.phtml';


    }

}