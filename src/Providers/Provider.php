<?php
/**
 * Created by PhpStorm.
 * User: Niko
 * Date: 02.01.2019
 * Time: 23:16
 */

namespace App\Providers;


interface Provider
{
    /**
     * @return mixed
    */
    public function getCurrenciesList();

    /**
     * @param $from
     * @param $to
     * @param $amount
     * @return mixed
     */
    public function getExchange( $from, $to, $amount);

    /**
     * @return mixed
     */
    public function saveRates();


    /**
     * @param $from
     * @param $to
     * @return mixed
     */
    public function getExchangeRate( $from, $to);
}