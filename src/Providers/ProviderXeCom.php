<?php
/**
 * Created by PhpStorm.
 * User: Niko
 * Date: 03.01.2019
 * Time: 1:21
 */

namespace App\Providers;
use App\Config\AppConfig;
use Couchbase\Exception;

class ProviderXeCom implements Provider
{
    const Account_ID="ua.photos704999329";
    const API_KEY='nhbuv4b9gckosekcbsailgqme9';
    const CURRENCIES_FILE = 'XeCom_currencies';
    /**
     * @return mixed
     */
    public function getCurrenciesList()
    {
        $config = new AppConfig();
        // TODO: Implement getCurrenciesList() method.
        //get currencies list from file,
        //file saved from provider and renewed once a day

        //check if file exists and updated not earlier then today
        if(file_exists($config->getProvidersInfoPath().$this::CURRENCIES_FILE))
        {
            $currencies = file_get_contents($config->getProvidersInfoPath().$this::CURRENCIES_FILE);
            $currencies = (array) json_decode($currencies,true);
            if($currencies['updatedAt']-time() < 3600*24)
                return $currencies;
        }

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
            CURLOPT_USERPWD => $this::Account_ID.":".$this::API_KEY,
            CURLOPT_URL => "https://xecdapi.xe.com:443/v1/currencies",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 15,
        ));

        $response = curl_exec($curl);
        $http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $err = curl_error($curl);
        $response_array = (array) json_decode($response,true);
        $response_array['updatedAt'] = time();
        $responce_to_file = json_encode($response_array);
        file_put_contents($config->getProvidersInfoPath().$this::CURRENCIES_FILE, $responce_to_file);

        curl_close($curl);

        return $response_array;
    }

    /**
     * @param $from
     * @param $to
     * @param $amount
     * @return mixed
     */
    public function getExchange($from, $to, $amount)
    {
        $url = "https://xecdapi.xe.com/v1/convert_from.json/?from=$from&to=$to&amount=$amount";

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_HTTPAUTH => CURLAUTH_BASIC,
            CURLOPT_USERPWD => $this::Account_ID.":".$this::API_KEY,
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_CONNECTTIMEOUT => 5,
            CURLOPT_TIMEOUT => 15,
        ));

        $response = curl_exec($curl);
        //$http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        //$err = curl_error($curl);
        $response_array = (array) json_decode($response,true);

        //save rates TODO
        curl_close($curl);

        return $response_array['to'][0]['mid'];
    }


    /** Save exchange rate for later use
     * @param $from
     * @param $to
     * @return mixed
     */
    public function getExchangeRate( $from, $to)
    {
        //save to json, format ['FromTo'=>rate,...]

    }

    /**
     * @return mixed
     */
    public function saveRates()
    {
        // TODO: Implement saveRates() method.
    }
}