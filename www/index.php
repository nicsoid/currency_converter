<?php
/**
 * Created by PhpStorm.
 * User: Niko
 * Date: 02.01.2019
 * Time: 13:00
 */

require_once __DIR__ . '/../vendor/autoload.php';

use App\Model;
use App\Controller;
//select route
$request = isset( $_REQUEST['route'] ) && trim( $_REQUEST['route'] ) ? trim( $_REQUEST['route'] ) : 'index';
$action='index';
//select controller
if( $request ) {

    // -----
    switch( $request ) {
        case 'index':
            $controller = 'IndexController';
            break;
        case 'providers':
            $controller = 'ProvidersController';
            break;
        case 'convert':
            $controller = 'IndexController';
            $action = 'convert';
            break;
        default:
            $controller = 'IndexController';
            break;
    }
} else {
    $controller = 'Error';
}

$controller='App\Controller\\'.$controller;
$app = new $controller();
$app->$action();


